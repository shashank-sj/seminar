% Architecture-Aware Approximate Computing
% Balamurali M\break S7 CSE\break Roll 21
% Supervised by Prof. Unnikrishnan K 

# Introduction

## Approximate Computing
>Increase performance and energy efficiency at the cost of accuracy

- Applicable where approximate result is sufficient
- Eg : Google, video compression
- K-means clustering saves 50 times more energy when 5% error is allowed
- Give tolerable error% along with input
- Applications : ML, signal processing, multimedia processing

## How is it done? and what is architecture-aware??
- Many ways exist such as loop perforation, reducing bits
- Take error bound along with input
- Use data access skipping to perform approximation
- Improve approximation by skipping costliest data accesses

# Target Manycore Architecture, Data Access and Different Localities

## Network on Chip
![Representation of NoC](images/noc.png)

- 7 different localities based on hit/miss.  
L1 hit, L2 hit (SQ), L2 hit (DQ), row-buffer hit (SQ), row-buffer hit (DQ), row-buffer miss (SQ), and row-buffer miss (DQ)

- C~1~, C~2~ .. C~7~
- C~i~ is faster to access than C~i~ + 1

## Data Access Drop
- Fine grained control
- 0 is used rather than fetching data
- Skip the right amount of data access within bound
- Drop costliest accesses first. (High C~i~)

# Benchmarks and Evaluation

## Benchmarks and Experimental setup
![Data locality for different workloads](images/locality.png)

- Experiments conducted using a cycle-accurate manycore
  simulator
- ImgSmooth and SSI have good locality
- Barnes-Hut and x264 have poor locality

## Evaluation
![](images/latPerfImp.png)

## Evaluation 
![](images/energyPerfImp.png)

## Evaluation 
- Skip data in respective categories randomly
- Energy benefits > Performance

## Is all this really needed?
![architecture aware vs random skipping](images/4.png)

## Quality Metric
![](images/5.png)

- Error increases as more categories dropped
- Not consistent for different executions of same workload

![](images/6.png)

# Slicing

## Why is it needed?

- To maximize performance
- To stay within a specified error bound

## Terminologies
Slice:            set of data & control dependencies
Forward Slicing:  Start from top towards dependants
Backward Slicing: Start from bottom towards dependencies

## Algorithm (in simple words)
Complexity : O(NM^2^)

1. Start backward slice for each output
2. for each LHS in bwd slice
3.	do fwd slice & see if it is not in bwd slice already
4.		if so, mark as not approximatable
5.		for the rest, calculate cost with respect to
		target architecture as they are
		approximatable
6. Afterwards, we get approximation sets for each output
   element
7. From set, identify the k most beneficial output elem to
   drop where k determined by total number of output elements &
   acceptable error value specified by user

Cost is evaluated using Cache Miss Estimation scheme which
is found to be 89% accurate

## Example

![Sample Program](images/15.png)

## Results

![](images/9.png)

-----
## Results
- Error bound = 2%
  Random strategy: 3.6%
  Slicing:         8.8%

- Error bound = 6%
  Random strategy: 12.9%
  Slicing:         21.5%

# Conclusion

## Conclusion
- Investigated benefits of skipping data accesses
- For given bound, found costliest data accesses
- Achieved 27% additional performance improvement (random
  drops)
- Slicing-based approach gave us even more improvements

##
> Thank you

<!-- vim:set tw=60: -->

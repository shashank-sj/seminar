---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: default
---
# Yawn: A CPU Idle-state Governor for Datacenter Applications

## Abstract

Idle-state governors partially turn off idle CPUs, allowing them to go to
states known as idle-states to save power.  Exiting from these idle-sates,
however, imposes delays on the execution of tasks and aggravates tail latency.
Menu, the default idle-state governor of Linux, predicts periods of idleness
based on the historical data and the disk I/O information to choose proper
idle-sates. Our experiments show that Menu can save power, but at the cost of
sacrificing tail latency, making Menu an inappropriate governor for data
centers that host latency-sensitive applications. In this paper, we present the
initial design of Yawn, an idle-state governor that aims to mitigate tail
latency without sacrificing power. Yawn leverages online machine learning
techniques to predict the idle periods based on information gathered from all
parameters affecting idleness, including network I/O, resulting in more
accurate predictions, which in turn leads to reduced response times.
Preliminary benchmarking results demonstrate that Yawn reduces the 99 th
latency percentile of Memcached requests by up to 40%.

# Proof-of-Play: A Novel Consensus Model for Blockchain-based Peer-to-Peer Gaming System

## Abstract

Data storage in peer-to-peer (P2P) games in a perfect applications scenario for
blockchain.  However, suffering from high transaction cost and latency,
proof-of-work (PoW) becomes the bottleneck of blockchain games.  Many attempts
have been made to overcome various limitations of blockchain for P2P games, but
many of them require modifying the game itself to be compatible with a
blockchain solution. These overheads often bring new undesirable results to
deal with. In this paper, we propose Proof-of-Play, a novel consensus model, to
address these issues with a blockchain naturally integrated with P2P games,
with minimum intervene to the game. The ultimate goal is to create a secure and
fully decentralized architecture to transform a game being
community-sustainable.

# WatchTower: Fast, Secure Mobile Page Loads Using Remote Dependency Resolution

## Abstract

Remote dependency resolution (RDR) is a proxy-driven scheme for
reducing mobile page load times; a proxy loads a requested page using a local
browser, fetching the page’s resources over fast proxy-origin links instead of
a client’s slow last-mile links. In this paper, we describe two fundamental
challenges to efficient RDR proxying: the increasing popularity of encrypted
HTTPS content, and the fact that, due to time-dependent network conditions and
page properties, RDR proxying can actually increase load times. We solve these
problems by introducing a new, secure proxying scheme for HTTPS traffic, and by
implementing WatchTower, a selective proxying system that uses dynamic models
of network conditions and page structures to only enable RDR when it is
predicted to help.  WatchTower loads pages 21.2%–41.3% faster than
state-of-the-art proxies and server push systems, while preserving end-to-end
HTTPS security.

<!-- vim:set tw=79: -->

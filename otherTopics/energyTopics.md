---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: default
---

# A Taxonomy and Future Directions for Sustainable Cloud Computing: 360 Degree View

The cloud-computing paradigm offers on-demand services over the Internet and
supports a wide variety of applications. With the recent growth of Internet of
Things (IoT)--based applications, the use of cloud services is increasing
exponentially. The next generation of cloud computing must be energy efficient
and sustainable to fulfill end-user requirements, which are changing
dynamically. Presently, cloud providers are facing challenges to ensure the
energy efficiency and sustainability of their services. The use of a large
number of cloud datacenters increases cost as well as carbon footprints, which
further affects the sustainability of cloud services. In this article, we
propose a comprehensive taxonomy of sustainable cloud computing. The taxonomy
is used to investigate the existing techniques for sustainability that need
careful attention and investigation as proposed by several academic and
industry groups. The current research on sustainable cloud computing is
organized into several categories: application design, sustainability metrics,
capacity planning, energy management, virtualization, thermal-aware scheduling,
cooling management, renewable energy, and waste heat utilization. The existing
techniques have been compared and categorized based on common characteristics
and properties. A conceptual model for sustainable cloud computing has been
presented along with a discussion on future research directions.

# Sustainable Offloading in Mobile Cloud Computing: Algorithmic Design and Implementation

Mobile Cloud Computing (MCC) has been extensively explored to be applied as a
vital tool to enhance the capabilities of mobile devices, increasing computing
power, expanding storage capacity, and prolonging battery life. Offloading
works as the fundamental feature that enables MCC to relieve task load and
extend data storage through an accessible cloud resource pool. Several
initiatives have drawn attention to delivering MCC-supported energy-oriented
offloading as a method to cope with a lately steep increase in the number of
rich mobile applications and the enduring limitations of battery technologies.
However, MCC offloading relieves only the burden of energy consumption of
mobile devices; performance concerns about Cloud resources, in most cases, are
not considered when dynamically allocating them for dealing with mobile tasks.
The application context of MCC, encompassing urban computing, aggravates the
situation with very large-scale scenarios, posing as a challenge for achieving
greener solutions in the scope of Cloud resources. Thus, this article gathers
and analyzes recent energy-aware offloading protocols and architectures, as
well as scheduling and balancing algorithms employed toward Cloud green
computing. This survey provides a comparison among system architectures by
identifying their most notable advantages and disadvantages. The existing
enabling frameworks are categorized and compared based on the stage of the task
offloading process and resource management types, describing current open
challenges and future research directions.

# Architecture-Aware Approximate Computing

Deliberate use of approximate computing has been an active research area
recently. Observing that many application programs from different domains can
live with less-than-perfect accuracy, existing techniques try to trade off
program output accuracy with performance-energy savings. While these works
provide point solutions, they leave three critical questions regarding
approximate computing unanswered, especially in the context of
dropping/skipping costly data accesses: (i) what is the maximum potential of
skipping (i.e., not performing) data accesses under a given inaccuracy bound?;
(ii) can we identify the data accesses to drop randomly, or is being
architecture aware (i.e., identifying the costliest data accesses in a given
architecture) critical?; and (iii) do two executions that skip the same number
of data accesses always result in the same output quality (error)? This paper
first provides answers to these questions using ten multithreaded workloads,
and then, motivated by the negative answer to the third question, presents a
program slicing-based approach that identifies the set of data accesses to drop
such that (i) the resulting performance/energy benefits are maximized and (ii)
the execution remains within the error (inaccuracy) bound specified by the
user. Our slicing-based approach first uses backward slicing and then forward
slicing to decide the set of data accesses to drop. Our experimental
evaluations using ten multithreaded workloads show that, when averaged over all
benchmark programs we have, 8.8% performance improvement and 13.7% energy
saving are possible when we set the error bound to 2%, and the corresponding
improvements jump to 15% and 25%, respectively, when the error bound is raised
to 4%.

# What I did

The `komascript` section at top of main was initially seperate file.
It could have been like the `\` prefixed lines that follow it.
I just thought this was cleaner.

# Tools used

`pandoc` - duh
`pdftoppm` - convert pdf slides to images

# LATEX explanation

- `\fancyhf{}` : to clear header and footer
- `\addtocontents{toc}{\textbf{Abstract}~\hfill\textbf{iii}\par}` : add
  arbitrary things to table of contents
- `\addcontentsline{toc}` is similar to above but adds page number automatically
according to unit

# Protip

Use offline documentation like `zeal` for stuff you don't understand


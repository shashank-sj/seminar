---
documentclass: report
geometry:
- top=25mm
- left=30mm
- right=30mm
indent: 12.5mm
linestretch: 1.5
papersize: a4
pagestyle: headings
mainfont: Times New Roman
fontsize: 12pt

---

# ABSTRACT

Approximate computing has huge potential in improving performance and energy
savings for certain application workloads where accuracy can be sacrificed. 
We investigate the benefits of being architecture-aware using a technique of
data skipping. This is done by running ten different workloads in a many-core
simulator. We see that being architecture-aware gives more performance (and
energy savings) over randomly skipping data accesses. We also investigate
whether skipping same number of data accesses in such a technique always
results in the same error %. From the negative answer to the previous
question, we devise a new slicing-based strategy to maximise performance and
stay within bounds specified. Experimentally, we find that slicing based
approach gives us improvements and consistent results compared to previous
effort. We also see many other ways, we can improve the slicing based
approach.  
**Key terms** : Approximate computing, Compiler, manycore system
